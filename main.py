import json
from actions import parse_action
games = {}

with open("examples/example7/Player.log") as f:
    contents = f.readlines()
    count = 0
    grab_next = False

    for line in contents:
        count += 1
        if grab_next is True and "{" in line:
            games = parse_action(line)
            data = json.loads(line) 
            grab_next = False

        if "Match to" in line: 
            grab_next = True
f.close()