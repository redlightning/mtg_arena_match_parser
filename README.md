# mtg_arena_match_parser

Parses Player.log files from Magic the Gathering - Arena to display match plays and actions

You must have extended logging turned on under your account in Arena.

Currently works only for 2 player games.

## Setup

```bash
pip install -r requirements.txt
```

## Schema
```json
{
    "matches": [
        {
            "id": int,
            "timestamp": long,
            "players": [
                {
                    "name": "string",
                    "seat": int,
                    "cards": [
                        {
                            "name": "string",
                            "instances": [
                                { 
                                    int,
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
}
```