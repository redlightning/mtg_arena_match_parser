import os
import json
import requests
from time import sleep

cache = {}

# Load the cache file
def loadCache():
    with open("cache/scryfall.com.json", "r") as read_file:
        cache = json.load(read_file)
        read_file.close()

# Converts the card's ID number to a text name
# card: a gameObject dictionary from the Player.log file
def getCardInfo(card):
    # Check the cache first
    if len(cache) == 0:
        loadCache()

    if str(card['name']) in cache:
        return cache[str(card['name'])]
    else:
        data = {}

        response = callScryfall(str(card['name']))
        if response.status_code == 200:
            data = json.loads(response.text)
        else:
            # Try grpId
            response = callScryfall(str(card['grpId']))
            if response.status_code == 200:
                data = json.loads(response.text)
            else: 
                # Try overlayGrpId
                response = callScryfall(str(card['overlayGrpId']))
                if response.status_code == 200:
                    data = json.loads(response.text)
                else:
                    print('Request failed: ' + str(response.status_code) + " for card " + str(card['name']))
                    return {}

        cache[str(card['name'])] = data
        # print("Got " + data['name'])
        os.makedirs("cache", exist_ok=True)
        with open('cache/scryfall.com.json', 'w') as write_file:
            json.dump(cache, write_file)
            write_file.close()
        return data

# Use a GET call agaisnt the Scryfall API to get card information
def callScryfall(id):
    sleep(0.1)
    response = requests.get('https://api.scryfall.com/cards/arena/' + id)
    return response
