import json
import os
from scryfall import getCardInfo
from datetime import datetime

games = {}
match = {}
reserved_players = {}
parsedZones = {}
match_id = ''
timestamp = 0

def parse_action(json_line):
    data = json.loads(json_line)

    # Client Authentication
    if "authenticateResponse" in data:
        print("Authentication")
    
    # Match State Changed
    if "matchGameRoomStateChangedEvent" in data:
        game_room_info = data['matchGameRoomStateChangedEvent']['gameRoomInfo']
        state_type = game_room_info['stateType']
        
        # Match Started/Playing
        if state_type == 'MatchGameRoomStateType_Playing':
            # Get player and match info
            global reserved_players
            global match_id
            global timestamp 
            # global games
            global match

            reserved_players = game_room_info['gameRoomConfig']['reservedPlayers']
            match_id = game_room_info['gameRoomConfig']['matchId']
            timestamp = int(str(data['timestamp']))
            match['id'] = match_id
            match['timestamp'] = timestamp

            players = {}
            for player in reserved_players:
                current_player = {'name': player['playerName'], 'id': player['systemSeatId'], 'cards': {}}
                players[player['playerName']] = current_player
                print(player['playerName'])
            match['players'] = players
            games[match_id] = match

            print("Match " + str(match_id) + " started at " + str(datetime.fromtimestamp(timestamp/1000.0)))
            # TODO Grab additional player and match metadata. See MatchGameRoomStateType_Playing.json file
            # print(reserved_players)

        # Match Complete
        if 'MatchGameRoomStateType_MatchCompleted' == state_type:
            finalMatchResult = game_room_info['finalMatchResult']
            resultList = finalMatchResult['resultList'] # TODO why a list?
            result = resultList[0]
            scope = result['scope']
            gameResult = result['result']
            winningTeamId = result['winningTeamId']
            reason = result['reason']
            print(get_player_name(winningTeamId) + " won, reason: " + reason)
            print(match)
            os.makedirs("matches", exist_ok=True)
            with open("matches/" + match_id + '.json', 'w') as write_file:
                json.dump(match, write_file, indent=4)
                write_file.close()
            #TODO Add result to match dictonary
        # print(gameRoomInfo)
    
    elif 'greToClientEvent' in data:
        # Client events
        greToClientEvent = data['greToClientEvent']
        greToClientMessages = greToClientEvent['greToClientMessages']
        for message in greToClientMessages:
            if message['type'] == 'GREMessageType_DieRollResultsResp':
                # print("Die roll")
                # TODO Show die roll results
                pass
            elif message['type'] == 'GREMessageType_GameStateMessage':
                # Game state has changed, usually due to a player action
                gameStateMessage = message['gameStateMessage']
                # Match is ready to start
                if 'GameStateType_Full' == gameStateMessage['type']:
                    # Get the instance IDs for each player's library
                    pass
                # Something changed    
                if 'GameStateType_Diff' == gameStateMessage['type']:
                    gameStateId = message['gameStateId']

                    # Actions available for the player to take
                    if 'actions' in gameStateMessage:
                        #TODO This would be interesting to look at to see what might have been
                        actions = gameStateMessage['actions']  
                    else:
                        print("No Actions: " + gameStateMessage) 

                    # Game objects: playing a card, using an ability, etc
                    if 'gameObjects' in gameStateMessage:
                        gameObjects = gameStateMessage['gameObjects']

                        # Get the zones (hand, battlefield, etc)
                        if 'zones' in gameStateMessage:
                            zones = gameStateMessage['zones']
                            for zone in zones:
                                parsedZones[zone['zoneId']] = zone['type']
                        # else:
                        #     # TODO What are these? Changing phases?
                        #     print("GAME STATE: " + str(gameStateMessage))
                        #     exit()

                        for gameObject in gameObjects:
                            current_match = games[match_id]
                            owner_seat_id = gameObject['ownerSeatId']
                            current_player_name = get_player_name(owner_seat_id)
                            current_player = {}
                            for player_key in current_match['players']:
                                player = current_match['players'][player_key]
                                if player['name'] == current_player_name:
                                    current_player = player

                            if 'GameObjectType_Ability' == gameObject['type']:
                                # TODO Handle abilities
                                # instanceId, grpId, zoneId, visibility, ownerSeatId, controllerSeatId, objectSourceGrpId, parentId
                                # print("Ability " + str(gameObject['objectSourceGrpId']))
                                pass
                            elif 'GameObjectType_Card' == gameObject['type']:
                                if 'name' in gameObject:
                                    cardInfo = getCardInfo(gameObject)
                                    cards = current_player['cards']

                                    if 'name' in cardInfo:
                                        # print(current_player['name'] + ' plays ' + cardInfo['name'] + "(" + str(gameObject["instanceId"]) + ") to " + parsedZones[gameObject['zoneId']] )       
                                        cards[str(gameObject["instanceId"])] = cardInfo['name']
                                    else:
                                        print(current_player['name'] + ' plays ' + str(gameObject['name']) + "(" + str(gameObject["instanceId"]) + ") to " + parsedZones[gameObject['zoneId']])
                                        cards[str(gameObject["instanceId"])] = str(gameObject['name'])
                                    # instanceId, grpId, type, visibility, cardTypes, abilities, overlayGrpId
                                    # TODO Map abilities
                                    # TODO Use instanceId to track unique copies of a card?
                                else:
                                    print("UNKNOWN CARD: " + str(gameObject))
                            elif 'GameObjectType_Token' == gameObject['type']:
                                if 'name' in gameObject:
                                    cardInfo = getCardInfo(gameObject)
                                    if 'name' in cardInfo:
                                        # print(current_player['name'] + ' creates token ' + cardInfo['name'] + " token to " + parsedZones[gameObject['zoneId']] )
                                        pass 
                                    else:
                                        print(current_player['name'] + ' creates token ' + str(gameObject['name']) + " to " + parsedZones[gameObject['zoneId']] )
                                        pass
                                    # power, toughness, color, abilityOriginalCardGrpIds, baseSkinCode,subtypes, skinCode, cardTypes, parentId, hasSummoningSickness
                                    # instanceId, grpId, type, visibility, cardTypes, abilities, overlayGrpId
                                    # TODO Map abilities
                                else:
                                    print("UNKNOWN TOKEN: " + str(gameObject))
                            elif 'GameObjectType_RevealedCard' == gameObject['type']:
                                if 'name' in gameObject:
                                    cardInfo = getCardInfo(gameObject)
                                    cards = current_player['cards']
                                    if 'name' in cardInfo:
                                        # print(current_player['name'] + ' reveals ' + cardInfo['name'] + "(" + str(gameObject["instanceId"]) + ")" )
                                        cards[str(gameObject["instanceId"])] = cardInfo['name']
                                    else:
                                        print(current_player['name'] + ' reveals ' + str(gameObject['name']) + "(" + str(gameObject["instanceId"]) + ") to " + parsedZones[gameObject['zoneId']])
                                        cards[str(gameObject["instanceId"])] = str(gameObject['name'])

                                    # power, toughness, color, viewers, subtypes, cardTypes
                                    # instanceId, grpId, type, visibility, cardTypes, abilities, overlayGrpId
                                    # TODO Map abilities
                                else:
                                    print("REVEALED UNKNOWN CARD: " + str(gameObject))
                            elif 'GameObjectType_MDFCBack' == gameObject['type']: # TODO What is MDFCBack? Is this Opening draw?
                                cardInfo = getCardInfo(gameObject)
                                cards = current_player['cards']
                                if 'name' in cardInfo:
                                    #print(current_player['name'] + ' moves ' + cardInfo['name'] + "(" + str(gameObject["instanceId"]) + ") to " + parsedZones[gameObject['zoneId']] )
                                    cards[str(gameObject["instanceId"])] = cardInfo['name']
                                else:
                                    print(current_player['name'] + ' moves ' + str(gameObject['name']) + "(" + str(gameObject["instanceId"]) + ") to " + parsedZones[gameObject['zoneId']])
                                    cards[str(gameObject["instanceId"])] = str(gameObject['name'])
                            else:
                                print("UNKNOWN TYPE: " + str(gameObject))
                                exit()

                    elif 'turnInfo' in gameStateMessage:
                        # Turn info, like changing of phases
                        turnInfo = gameStateMessage['turnInfo']
                        # priorityPlayer, decisionPlayer, nextPhase, nextStep
                        if 'turnNumber' in turnInfo:
                            # print("Active Player: " + str(turnInfo['activePlayer']) + ", turn: " + str(turnInfo['turnNumber']) + ", phase: " + turnInfo['phase'])
                            pass
                        else:
                            print(turnInfo)
                    
                    elif 'gameInfo' in gameStateMessage:
                        # Game start/end messages
                        gameInfo = gameStateMessage['gameInfo']
                        stage = gameInfo['stage']
                        # 'matchID, gameNumber, type, variant, matchState, matchWinCondition, maxTimeoutCount, maxPipCount, timeoutDurationSec, superFormat, mulliganType
                        matchResults = gameInfo['results']
                        #scope, result, winningTeamId, reason
                        deckConstraintInfo = gameInfo['deckConstraintInfo']
                        # minDeckSize, maxDeckSize, maxSideboardSize
                    elif 'update' in gameStateMessage:
                        # print(gameStateMessage['update'])
                        pass
                    else:
                        print("No gameObjects: " + str(gameStateMessage))
    else:
        if 'timestamp' in data:
            timestamp = data['timestamp']
        # print(data)
    return games

# Get player name from seat id
def get_player_name(seat_id):
    current_match = games[match_id]
    for player_key in current_match['players']:
        current_player = current_match['players'][player_key]
        if seat_id == int(current_player['id']):
            return current_player['name']
    return seat_id